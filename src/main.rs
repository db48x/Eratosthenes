#![allow(non_snake_case)]
#![allow(non_ascii_idents)]
use structopt::StructOpt;

use Eratosthenes::*;

#[derive(StructOpt, Debug)]
#[structopt(name = "Eratosthenes")]
struct Opt {
    /// Compute all primes smaller than this limit
    n: usize
}

fn main() {
    let opt = Opt::from_args();
    println!("largest prime less than {} is {}", opt.n, sieve(opt.n).last().unwrap());
}
