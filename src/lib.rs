pub fn sieve(n: usize) -> Vec<u64> {
    let mut numbers = vec![true; n as usize+1];
    let mut primes: Vec<u64> = vec![];
    let sqrt = f64::sqrt(n as f64).floor() as usize;

    numbers[0] = false;
    numbers[1] = false;

    let mut prime = 2;
    loop {
        for multiple in (prime*prime..=n).step_by(prime) {
            numbers[multiple] = false;
        }

        for candidate in prime+1..=n {
            if numbers[candidate] {
                prime = candidate;
                break;
            }
        }
        if prime > sqrt {
            break;
        }
    };

    for candidate in 0..=n {
        if numbers[candidate] {
            primes.push(candidate as u64);
        }
    }

    return primes;
}
