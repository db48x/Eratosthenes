use criterion::{black_box, criterion_group, criterion_main, Criterion, BenchmarkId};
use Eratosthenes::sieve;

pub fn bench_sieve(c: &mut Criterion) {
    let mut group = c.benchmark_group("sieve");
    for count in &[100, 1000, 10000, 100000, 1000000] {
        group.bench_with_input(BenchmarkId::from_parameter(count),
                               count,
                               |b, &count| {
                                   b.iter(|| sieve(black_box(count)))
                               });
    }
}

criterion_group!(benches, bench_sieve);
criterion_main!(benches);
